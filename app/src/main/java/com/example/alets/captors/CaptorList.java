package com.example.alets.captors;

import android.content.Context;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static android.content.Context.CAMERA_SERVICE;
import static android.content.Context.SENSOR_SERVICE;
import static android.hardware.Camera.Parameters.FLASH_MODE_AUTO;
import static android.hardware.Camera.Parameters.FLASH_MODE_ON;
import static android.hardware.camera2.CameraMetadata.FLASH_MODE_TORCH;


public class CaptorList extends Fragment implements SensorEventListener {

    private SensorManager mSensorManager;
    List<Sensor> msensorList;
    Sensor acelerometer,proximity;
    TextView sensorData;
    ImageView proximityImage;
    int treshHold = 15;
    float treshHoldMove = 1f;
    String textx="",texty="",textz="";
    Boolean flash = false;
    Float x = 0f,y= 0f,z= 0f;
    Float lastSome = 0f;
    private OnFragmentInteractionListener mListener;

    public CaptorList() {
        // Required empty public constructor
    }

    public static CaptorList newInstance() {
        CaptorList fragment = new CaptorList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);
        msensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        acelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        proximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        mSensorManager.registerListener(this, acelerometer , SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, proximity , SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_captor_list, container, false);
        TextView tv = v.findViewById(R.id.list);
        sensorData = v.findViewById(R.id.sensorData);
        proximityImage = v.findViewById(R.id.photo);

        String text = "";
        for (Sensor s: msensorList) {
            text+= s.getName();
            text+= "\n";

        }
        tv.setText(text);

        // Inflate the layout for this fragment
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            changeAcelerometer(sensorEvent);
        }
        if(sensorEvent.sensor.getType() == Sensor.TYPE_PROXIMITY ){
            changeProximity(sensorEvent);
        }

    }
    public void changeProximity(SensorEvent event){
                proximityImage.setVisibility( (event.values[0]==0) ?View.INVISIBLE : View.VISIBLE);

    }
    public  void changeAcelerometer(SensorEvent sensorEvent){
        String s = sensorEvent.sensor.getName();
        String text = s+"\n";
        Float some = 0f;


        float alpha = 0.8f;



        for (int i = 0; i < sensorEvent.values.length; i++){


          /*  gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
            linear_acceleration[2] = event.values[2] - gravity[2];*/

            text+= " vaule "+i+": "+ sensorEvent.values[i] + "\n";
            some += (sensorEvent.values[i]>0)?sensorEvent.values[i] : -sensorEvent.values[i];
        }
        some -= 9.5f;
        text+= some+"\n \n";
        //test direction

        Float difX  = sensorEvent.values[0]-x;
        if (Math.abs(difX)>treshHoldMove){
            textx = ((difX<0)? " right --->":" <---left ")+"\n";
        }
        text+= "x : " +textx;
         Float dify  = sensorEvent.values[1]-y;
        if (Math.abs(dify)>treshHoldMove) {
            texty  = ((dify>0) ? " up '''''' " : " down ______") + "\n";
        }
        text += "y : " +texty;
        Float difz  = sensorEvent.values[2]-z;
        if (Math.abs(difz)>treshHoldMove) {
            textz  = ((difz>0) ? " back ...... " : " front OOOOOOO") + "\n";
        }
        text += "z : " +textz;
        //test shake

        if(lastSome> some+ treshHold){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                flashState(flash);
                flash = !flash;
            }
        }
        x = sensorEvent.values[0];
        y = sensorEvent.values[1];
        z = sensorEvent.values[2];
        lastSome = some;
        int val = (int) (255* some)/15;
        sensorData.setText(text);
        sensorData.setBackgroundColor(Color.rgb(val, 255-val, 00));
    }

     @RequiresApi(api = Build.VERSION_CODES.M)
     public  void flashState (Boolean state){
         CameraManager cameraManager = (CameraManager)getContext().getSystemService(CAMERA_SERVICE);
         try {
             for (String cameraId : cameraManager.getCameraIdList()) {
                 CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);

                 Boolean hasflash = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                 if (hasflash != null && hasflash) {
                    cameraManager.setTorchMode(cameraId,state);
                 }
                 // Do something with the characteristics
             }
         } catch (CameraAccessException e) {
             e.printStackTrace();
         }
     }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
